import plotly
import plotly.graph_objects as go 
import pandas as pd;

excel_file = 'worksheet1.xlsx' # 打开excel文件

df = pd.read_excel(excel_file) # 获取数据

data = [go.Scatter(x = df['Year'], y = df['EU_Sales'])] # 选取数据

fig = go.Figure(data) # 画图
fig.show()

plotly.offline.plot(fig, filename='vgsalereport.html') # 转换成HTML网页形式